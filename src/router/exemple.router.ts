import {Router} from 'express'
import {Services} from "../services";

let exempleRouter: Router = Router();

exempleRouter.get('/', Services.exemple.service1, Services.exemple.service2);


export default exempleRouter;
