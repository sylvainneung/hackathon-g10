import {Router} from 'express'
import {Services} from "../services";

let authenticationRouter: Router = Router();

authenticationRouter.get('/me', Services.authentication.me);
authenticationRouter.post('/login', Services.authentication.login);
authenticationRouter.post('/signin', Services.authentication.signin);

export default authenticationRouter;
