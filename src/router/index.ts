'use strict';

import exempleRouter from "./exemple.router";
import authenticationRouter from "./authentication.router";

export const Routers = {
    exempleRouter : {
        prefix: '/exemple',
        router: exempleRouter
    },
    authentication: {
        prefix: '/auth',
        router: authenticationRouter
    }
};
