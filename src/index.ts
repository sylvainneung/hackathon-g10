'use strict';
import {Users} from "./db/models/users.model";

require('dotenv').config();

import {Routers} from "./router";
import App from './server/App';



const port = process.env.SERVER_PORT || 4000;


for (let route in Routers) {
    App.use(Routers[route].prefix, Routers[route].router)
}

App.listen(port, async () => {
    // Sync every models here (you can change alter by force to regenerate each startup the database
    Users.sync({ alter: true }).then( () => console.log("Users model sync !"));
    console.log("Application start on port :" + port);
});
