'use strict';

import {Request, Response, NextFunction} from 'express';
import {Users} from "../db/models/users.model";

export const myService1 = async (req: Request, res: Response, next:NextFunction) => {
    Users.create({ firstName: "Pruno", lastName:"De" });

    console.log("myService 1");
    next();
};

export const myService2 = async (req: Request, res: Response, next:NextFunction) => {
    console.log("myService 2");
    res.status(200).json({message: "from servie2"})
};
