'use strict';

import {Request, Response, NextFunction} from 'express';
import {Users} from "../db/models/users.model";

const argon2 = require('argon2');
const jwt = require('jsonwebtoken');




// Call this method to check the token is correct (this method also set data from decoded token to req.authToken)
export const checkToken = async (req: Request, res: Response, next:NextFunction) => {
    if (typeof req.headers.authorization !== "undefined") {
        let token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        req["authToken"] = decoded;
        next();
    } else {
        res
            .status(401)
            .json({
                message: "No authorization set in your header",
                data: "send token with Bearer authorization"
            })
    }
};

export const me = async (req: Request, res: Response, next:NextFunction) => {
    if (typeof req.headers.authorization !== "undefined") {
        let token = req.headers.authorization.split(" ")[1];

        try {
            const decoded = jwt.verify(token, process.env.JWT_SECRET);
            res
                .status(200)
                .json({
                    message: "Vos informations personnels",
                    data: decoded
                })
        } catch (e){
            res
                .status(401)
                .json({
                    message: "Token erreur",
                    data: e
                })
        }

    } else {
        res
            .status(401)
            .json({
                message: "Invalid token",
                data: "send token with Bearer authorization"
            })
    }
};

export const login = async (req: Request, res: Response, next:NextFunction) => {
    const {email, password} = req.body;

    if(email && password) {
        try {
            const userExist = await Users.findOne({
                where: {
                    email : email,
                }
            });

            if(userExist === null) {
                res
                    .status(200)
                    .json({
                        message: "Utilisateur n'existe pas ",
                        data: "Aucun utilisateur pour ces identifiants"
                    })
            } else {
                let hash = userExist.toJSON().password;

                try {
                    if (await argon2.verify(hash, password)) {

                        delete userExist.toJSON().password;
                        const token = jwt.sign({
                            exp: Math.floor(Date.now() / 1000) + (60 * 60),
                            data: userExist.toJSON()
                        }, process.env.JWT_SECRET);
                        // password match
                        res
                            .status(200)
                            .json({
                                message: "Logged with success",
                                data: token
                            })
                    } else {
                        // password did not match
                        res
                            .status(400)
                            .json({
                                message: "Identifiant invalide",
                                data: "Mot de passe incorrect"
                            })
                    }
                } catch (err) {
                    res
                        .status(500)
                        .json({
                            message: "Oups une erreur interne est survenue. Veuillez réessayer plus tard",
                            data: err
                        })
                }
            }
        } catch(e) {
            res.status(400).json({
                message: "Erreur lors de la recherche de l'utilisateur",
                data: e
            })
        }
    } else {
        res
            .status(400)
            .json({message: "Identifiants incorrect", data: "Vérifiez vos informations"})
    }
};

export const signin = async (req: Request, res: Response, next:NextFunction) => {
    const {firstName, lastName, email, phoneNumber, password} = req.body;

    if(firstName && lastName && email && phoneNumber && password) {

        try {
            const userExist = await Users.findOne({
                where: {
                    firstName: firstName,
                    lastName: lastName,
                    email : email,
                }
            });


            if(userExist === null) {

                try {

                    const hash = await argon2.hash("password");
                    Users
                        .create({
                            firstName: firstName,
                            lastName: lastName,
                            email : email,
                            phoneNumber: phoneNumber,
                            password: hash
                        })
                        .then( (data) =>{
                            delete data.toJSON().password;
                            const token = jwt.sign({
                                exp: Math.floor(Date.now() / 1000) + (60 * 60),
                                data: data.toJSON()
                            }, process.env.JWT_SECRET);

                            res.status(200).json({message: "token generated with success", data: token})
                        })
                        .catch(err => {
                            res
                                .status(400)
                                .json({
                                    message: err
                                })
                        });

                } catch(e) {
                    res.status(400).json({
                        message: "Erreur mot de passe",
                        data: e
                    })
                }

            } else {
                res.status(400).json({
                    message: "Ces identifiants sont déjà utilisés",
                    data: "User existe déjà"
                })
            }
        } catch(e){
            res
                .status(400)
                .json(e)
        }

    } else {
        let errors = {};

        if(!firstName) {
            errors["firstName"] = "Prénom invalide"
        }

        if(!lastName) {
            errors["lastName"] = "Nom invalide"
        }

        if(!email) {
            errors["email"] = "Email invalide"
        }

        if(!phoneNumber) {
            errors["phoneNumber"] = "Numéro de téléphone invalide"
        }

        if(!password) {
            errors["password"] = "Password invalide"
        }

        res.status(400).json({
            message: "Champs non remplis.",
            data: errors
        })
    }
};
