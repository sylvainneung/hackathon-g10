'use strict';

import {myService1, myService2} from "./exemple.service";
import {checkToken, login, me, signin} from "./authentication.service";

export const Services = {
    exemple: {
        service1: myService1,
        service2: myService2
    },
    authentication: {
        checkToken: checkToken, // this method can be use to check token on every routes
        login : login,
        signin: signin,
        me: me
    }
};
