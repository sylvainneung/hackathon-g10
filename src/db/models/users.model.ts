'use strict';
import {db} from "../index";

const {DataTypes, Model } = require('sequelize');


export class Users extends Model {
    public firstName: string;
    public lastName: string;
    public email: string;
    public phoneNumber: string;
    public password: string;
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}

Users.init({
    // Model attributes are defined here
    firstName: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    lastName: {
        allowNull: false,
        type: DataTypes.STRING
        // allowNull defaults to true
    },
    email: {
        allowNull: false,
        type: DataTypes.STRING
    },
    phoneNumber: {
        allowNull: false,
        type: DataTypes.STRING
    },
    password: {
        allowNull: false,
        type: DataTypes.STRING
    },
}, {
    timestamps: true,
    updatedAt: 'updateTimestamp',

    sequelize: db.sequelize, // We need to pass the connection instance
    modelName: 'Users' // We need to choose the model name
});



